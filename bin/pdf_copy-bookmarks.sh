#!/bin/sh
# by Klaatu
# GPLv3

## Purpose:
# 0. take bookmarks from one PDF
# 1. apply it to another PDF

## Requires:
# pdftk

## parse opts
VERBOSE=0
while [ True ]; do
if [ "$1" = "--help" -o "$1" = "-h" ]; then
    echo "0. take bookmarks from one PDF"
    echo "1. apply it to another PDF"
    echo " "
    echo "$0 [--verbose] [--input|--output] FILES"
    echo " "
    exit 0
elif [ "$1" = "--verbose" -o "$1" = "-v" ]; then
    VERBOSE=1
    shift 1
elif [ "$1" = "--input" -o "$1" = "-i" ]; then
    INPUT="$2"
    shift 2
elif [ "$1" = "--output" -o "$1" = "-o" ]; then
    OUTPUT="$2"
    shift 2
else
    break
fi
done

# fail faster
set -e

function get_bookmarks() {
    pdftk "${INPUT}" dump_data output bookmark.tmp.txt
}

function put_bookmarks() {
    mv "${OUTPUT}" tmp_"${OUTPUT}"
    pdftk tmp_"${OUTPUT}" update_info bookmark.tmp.txt output "${OUTPUT}"
}

function cleanup() {
    rm bookmark.tmp.txt
    rm tmp_"${OUTPUT}"
}

get_bookmarks
put_bookmarks
cleanup

exit 0
