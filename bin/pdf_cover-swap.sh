#!/bin/sh
# by Klaatu
# GPLv3

## Purpose:
# 0. takes a PDF a drop the first page
# 1. gets the first page of a different PDF
# 2. applies the page to the first PDF

## Requires:
# pdftk or pdftk-java

## parse opts
STRIP=0
VERBOSE=0
while [ True ]; do
    if [ "$1" = "--help" -o "$1" = "-h" ]; then
    echo "Purpose:"
    echo "0. take a PDF a drop the first page"
    echo "1. get the first page of a different PDF"
    echo "2. apply the page to the first PDF"
    echo " "
    echo "$0 [--verbose] [--body|--cover|--strip|--output] FILES"
    echo " "
    exit 0
elif [ "$1" = "--verbose" -o "$1" = "-v" ]; then
    VERBOSE=1
    shift 1
elif [ "$1" = "--cover" -o "$1" = "-c" ]; then
    COVER="$2"
    shift 2
elif [ "$1" = "--strip" -o "$1" = "-s" ]; then
    STRIP=1
    shift 1
elif [ "$1" = "--body" -o "$1" = "-b" -o "$1" = "-i" -o "$1" = "--input" ]; then
    BODY="$2"
    shift 2
elif [ "$1" = "--output" -o "$1" = "-o" ]; then
    OUTPUT="$2"
    shift 2
else
    break
fi
done

# fail faster
set -e

function get_cover() {
    [ "${VERBOSE}" -eq 1 ] && echo "Processing ${COVER}"
    pdftk "${COVER}" cat 1 output cover.tmp.pdf
}

function strip_cover() {
    pdftk "${BODY}" cat 2-end output body.tmp.pdf 
}

function put_cover() {
    pdftk cover.tmp.pdf body.tmp.pdf \
	  cat output "${OUTPUT}"
}

function cleanup() {
    rm cover.tmp.pdf
    rm body.tmp.pdf
}

get_cover
if [ "${STRIP}" -eq 1 ]; then
    strip_cover
else
    ln -s "${BODY}" body.tmp.pdf 
fi

put_cover
cleanup

exit 0
