#!/bin/sh
# by Klaatu
# GPLv3

## Requires
# ghostscript

## Purpose
# Removes all raster images from a PDF


function die() { echo "You must provide an input file."
		 exit
	       }

function img_rm() {
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 \
   -dPDFSETTINGS="/${SETTING}" -dNOPAUSE \
   -dBATCH -sOutputFile="${OUTPUT}" -dFILTERIMAGE \
   "${INPUT}"
}

## parse opts
while [ True ]; do
if [ "$1" = "--help" -o "$1" = "-h" ]; then
    echo "Purpose: Removes all raster images from a PDF"
    echo " "
    echo "$0 [--setting|--output|--input] foo.pdf"
    echo " "
    exit
elif [ "$1" = "--setting" -o "$1" = "-s" ]; then
    SETTING="$2"
    shift 2
elif [ "$1" = "--output" -o "$1" = "-o" ]; then
    OUTPUT="$2"
    shift 2
elif [ "$1" = "--input" -o "$1" = "-i" ]; then
    INPUT="$2"
    shift 2
else
    break
fi
done

[ "x${INPUT}" = "x" ] && die

[ "x${SETTING}" = "x" ] && SETTING="ebook"
[ "x${OUTPUT}" = "x" ] && OUTPUT="output.pdf"

img_rm

exit 0
